import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex';
import OperationList from '@/components/OperationList.vue'
import Vuetify from 'vuetify';


const localVue = createLocalVue();

localVue.use(Vuex);
localVue.use(Vuetify);

describe('Component OperationList', () => {
    let actions;
    let state;
    let getters;
    let store;

    beforeEach(() => {
        getters = {
            balance: () => 3
        };
        state= {
            operations: [
                {id: 1, description: 'Operation1', amount: 1, category: {label: 'Category'}},
                {id: 2, description: 'Operation2', amount: 2, category: {label: 'Category'}},
            ]
        };
        actions= {
            fetchOperations: jest.fn()
        };

        store = new Vuex.Store({
            state,
            actions,
            getters
        });
    });

    it('should display operations list', () => {
        const wrapper = shallowMount(OperationList, { store, localVue })
        expect(actions.fetchOperations).toHaveBeenCalled();
        let list = wrapper.findAll('.operation');
        expect(list.length).toBe(2);
        expect(list.at(0).text()).toEqual(expect.stringContaining('Operation1'));
    });
    

})