import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios';

Vue.use(Vuex)

const apiUrl = 'http://localhost:8000';

export default new Vuex.Store({
  state: {
    operations: [],
    categories: []
  },
  getters: {
    balance(state) {
      return state.operations.reduce((total, item) => total + item.amount, 0);
    }
  },
  mutations: {
    ADD_OPERATION(state, operation) {
      state.operations = [...state.operations, operation];
    },
    REMOVE_OPERATION(state, operation) {
      state.operations = state.operations.filter(item => item.id !== operation.id);
    },
    MODIFY_OPERATIONS(state, operations) {
      state.operations = operations;
    },
    MODIFY_CATEGORIES(state, categories) {
      state.categories = categories;
    }

  },
  actions: {
    async fetchOperations({commit}) {
      let response = await Axios.get(apiUrl+'/api/operation');
      commit('MODIFY_OPERATIONS', response.data);
    },
    async addOperation({commit}, operation) {
      let response = await Axios.post(apiUrl+'/api/operation', operation);
      commit('ADD_OPERATION', response.data);
    },
    async rmOperation({commit}, operation) {
      let response = await Axios.delete(apiUrl+'/api/operation/'+operation.id);
      commit('REMOVE_OPERATION', operation);
    },
    async fetchCategories({commit}) {
      let response = await Axios.get(apiUrl+'/api/category');
      commit('MODIFY_CATEGORIES', response.data);
    },
  }
})
